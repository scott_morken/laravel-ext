<?php namespace Smorken\Ext;

use Illuminate\Support\ServiceProvider;

class ExtServiceProvider extends ServiceProvider
{

    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->registerDatabaseExt();
    }

    protected function registerDatabaseExt()
    {
        $this->app->register(DatabaseServiceProvider::class);
    }
}
